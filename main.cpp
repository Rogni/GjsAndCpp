
#include <gtkmm.h>
#include "gjs_wrap.hpp"
#include <giomm.h>
#include <glibmm/extraclassinit.h>
#include <iostream>
#include "users_model.hpp"



int main(int argc, char ** argv)
{
    
    Glib::wrap_register_init();
    {
        UsersModel::register_gtype();
    }

    Gjs::Context context;

    context.eval_file("resource:///com/example/gjs/main.js");

    Glib::wrap_register_cleanup();
    return 0;
}