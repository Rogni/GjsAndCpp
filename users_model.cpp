#include "users_model.hpp"

User::User() : 
    Glib::ObjectBase("User"),
    m_firstname(*this, "firstname"),
    m_secondname(*this, "secondname")
{}

Glib::RefPtr<User> User::create(Glib::ustring firstname, Glib::ustring secondname)
{
    auto user = Glib::RefPtr<User>(new User);
    user->m_firstname.set_value(firstname);
    user->m_secondname.set_value(secondname);
    return user;
} 

GType UsersModel::gtype = 0;

UsersModel::UsersModel() : 
    Glib::ObjectBase("UsersModel"),
    Gio::ListModel(),
    Glib::ExtraClassInit(nullptr, nullptr, UsersModel::register_instance)
{
    
}

UsersModel::UsersModel(GObject* castitem) :
    Glib::ExtraClassInit(nullptr, nullptr, nullptr),
    Glib::Object(castitem)
{
    m_users = {
        User::create("Firstname1", "Secondname1"),
        User::create("Firstname2", "Secondname2")
    };
}

void UsersModel::register_instance(GTypeInstance   *instance,
            gpointer g_class)
{
    auto gobj = reinterpret_cast<GObject*>(instance);
    if (!gtype) {
        gtype = G_OBJECT_TYPE(gobj);
        Glib::wrap_register(gtype, UsersModel::wrap_new);
    } else {
        Glib::wrap_auto(gobj);
    }
}

Glib::ObjectBase * UsersModel::wrap_new(GObject *o)
{
    return new UsersModel(o);
}


void UsersModel::register_gtype()
{
    if (!gtype) {
        UsersModel dummy;
    }
}

GType UsersModel::get_item_type_vfunc()
{
    return User::get_type();
}

guint UsersModel::get_n_items_vfunc()
{
    return m_users.size();
}

gpointer UsersModel::get_item_vfunc(guint position) 
{
    auto obj = m_users[position];
    obj->reference();
    return obj->gobj();
}