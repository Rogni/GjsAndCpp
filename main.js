imports.gi.versions.Gtk = '3.0';
const Gtk = imports.gi.Gtk;
const Gio = imports.gi.Gio;
const GObject = imports.gi.GObject;

Gtk.init(null);

function create_model() {
    const UserModelType = GObject.type_from_name ('gtkmm__CustomObject_UsersModel') 
    var model = GObject.Object.newv(UserModelType, []);
    return model;
}


function create_row(user_object) {
    var row = new Gtk.ListBoxRow();
    var box = new Gtk.Box();
    var firsname_label = new Gtk.Label({
        label: user_object.firstname
    });
    var secondname_label = new Gtk.Label({
        label: user_object.secondname
    });
    
    row.add(box);
    box.add(firsname_label);
    box.add(secondname_label);
    row.show_all();
    return row;
}

var MainWindow = GObject.registerClass(
    {},
    class MainWindow extends Gtk.Window {
        _init(props = {}) {
            super._init(props);
            var list_box = new Gtk.ListBox();    
            list_box.bind_model(create_model(), create_row); 
            this.add(list_box);
            this.show_all();
            this.connect("delete-event", () => { Gtk.main_quit(); });

        }
    }
);

var win = new MainWindow({
    title: 'GjsAndCpp',
    resizable: true
});

Gtk.main();