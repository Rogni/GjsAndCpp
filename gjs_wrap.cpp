#include "gjs_wrap.hpp"

Gjs::Context::Context():
    m_context(gjs_context_new())
{

}

Gjs::Context::~Context()
{
    g_object_unref(gobj());
}

GjsContext * Gjs::Context::gobj()
{
    return m_context;
}


bool Gjs::Context::eval_file(const Glib::ustring & filename)
{
    GError * error = nullptr;
    int exit_code = 0;

    return gjs_context_eval_file(gobj(), filename.c_str(), &exit_code, &error);
}