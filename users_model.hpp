#pragma once

#include <giomm.h>
#include <glibmm/extraclassinit.h>
#include <vector>

class User: public Glib::Object
{
    Glib::Property<Glib::ustring> m_firstname;
    Glib::Property<Glib::ustring> m_secondname;
protected:
    User();
public:
    static Glib::RefPtr<User> create(Glib::ustring firstname, Glib::ustring secondname);
};


class UsersModel: 
    public Gio::ListModel,
    public Glib::ExtraClassInit,
    public Glib::Object    
{
    static GType gtype;
    std::vector<Glib::RefPtr<User>> m_users; 

    UsersModel();
    UsersModel(GObject* castitem);

    static void register_instance(GTypeInstance   *instance,
                gpointer g_class);

    static Glib::ObjectBase * wrap_new(GObject *o);

    

public:
    static void register_gtype();

    GType get_item_type_vfunc();

    guint get_n_items_vfunc();

    gpointer get_item_vfunc(guint position);

};
