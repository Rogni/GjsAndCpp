cmake_minimum_required(VERSION 3.18)
project(GjsAndCpp)

find_package(PkgConfig REQUIRED)

set(CMAKE_CXX_STANDARD 20)
set(CMAKE_CXX_STANDARD_REQUIRED ON)
set(CMAKE_CXX_EXTENSIONS OFF)

pkg_search_module(GTKMM REQUIRED IMPORTED_TARGET gtkmm-3.0)
pkg_search_module(GJS REQUIRED IMPORTED_TARGET gjs-1.0)

find_program(GLIB_COMPILE_RESOURCES NAMES glib-compile-resources REQUIRED)

set(GRESOURCE_C   gjs.gresource.c)
set(GRESOURCE_XML gjs.gresource.xml)
set(GRESOURCES 
    main.js
)

add_custom_command(
    OUTPUT ${GRESOURCE_C}
    WORKING_DIRECTORY ${CMAKE_CURRENT_SOURCE_DIR}/
    COMMAND ${GLIB_COMPILE_RESOURCES}
    ARGS
        --target=${CMAKE_CURRENT_BINARY_DIR}/${GRESOURCE_C}
        --generate-source
        ${GRESOURCE_XML}
    VERBATIM
    MAIN_DEPENDENCY ${CMAKE_CURRENT_SOURCE_DIR}/${GRESOURCE_XML}
    DEPENDS
        ${GRESOURCES}
)

add_custom_target(
        compile_gresource
        DEPENDS ${CMAKE_CURRENT_BINARY_DIR}/${GRESOURCE_C}
    )

set (SOURCES 
    main.cpp
    gjs_wrap.cpp
    users_model.cpp
    ${GRESOURCE_C}
)

add_executable(Main ${SOURCES})

target_link_libraries(Main PRIVATE PkgConfig::GTKMM)
target_link_libraries(Main PRIVATE PkgConfig::GJS)

set_source_files_properties(
    ${CMAKE_CURRENT_BINARY_DIR}/${GRESOURCE_C}
    PROPERTIES GENERATED TRUE
)