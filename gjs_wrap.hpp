#pragma once

#include <gjs/gjs.h>
#include <gtkmm.h>

namespace Gjs 
{
    class Context
    {
    public:
        Context();
        ~Context();

        GjsContext * gobj();     

        bool eval_file(const Glib::ustring & filename);
    private:
        GjsContext * m_context = nullptr;
    };
}